# mpopup

![](https://img.shields.io/badge/written%20in-C%2B%2B-blue)

A utility for making your own custom menus in a Windows 7+ jump list.

Download includes instructions and source code.


## Download

- [⬇️ mpopup-r1c.rar](dist-archive/mpopup-r1c.rar) *(32.64 KiB)*
- [⬇️ mpopup-r1b.rar](dist-archive/mpopup-r1b.rar) *(32.69 KiB)*
- [⬇️ mpopup-r1a.rar](dist-archive/mpopup-r1a.rar) *(30.33 KiB)*
